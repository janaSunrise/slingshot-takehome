const DEV_MODE = false;
const URL = DEV_MODE
  ? 'http://127.0.0.1:5000/api'
  : 'https://global-trie-sunrit.herokuapp.com/api';

module.exports = { URL };
