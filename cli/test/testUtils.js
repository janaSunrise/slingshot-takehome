// Functions
const generateRandomWord = () => {
  const alphabets = 'abcdefghijklmnopqrstuvwxyz';

  let wordLen = Math.floor(Math.random() * 5) + 2;

  let word = '';

  for (let i = 0; i < wordLen; i++) {
    word += alphabets[Math.floor(Math.random() * alphabets.length)];
  }

  return word;
};

// Get prefix of a random word from an array based on length

const getRandomPrefix = arr => {
  const word = arr[Math.floor(Math.random() * arr.length)];

  const prefixLen = Math.floor(Math.random() * (word.length - 1));

  return word.substring(0, prefixLen);
};

// https://stackoverflow.com/questions/67417566/choose-and-delete-random-word-from-array-in-javascript
const getAndPopWord = (arr, count) => {
    let words = [];
    let rand;

    for (let i = 0; i < count; i++) {
        rand = Math.floor(Math.random() * arr.length);
        words.push(arr.splice(rand, 1)[0]);
    }

    return words;
};

module.exports = {
  generateRandomWord,
  getRandomPrefix,
  getAndPopWord
};
