const { URL } = require('../config');

const {
  addWord,
  deleteWord,
  searchWord,
  autocompleteWord,
  getWords,
  resetTrie
} = require('../bin/utils');
const {
  generateRandomWord,
  getRandomPrefix,
  getAndPopWord
} = require('./testUtils');

// Words
const words = [];

for (let i = 0; i < 5; i++) {
  const word = generateRandomWord();

  if (words.includes(word)) {
    i--;
  } else {
    words.push(word);
  }
}

// Tests
describe('Reset to default', () => {
  test('Resetting', async () => {
    await resetTrie(URL);

    const { words: trieWords } = await getWords(URL);
    expect(trieWords).toEqual([]);
  });
});

describe('Add words', () => {
  words.forEach(word => {
    test(`Adding the word ${word}`, async () => {
      await addWord(URL, word);

      const { words: trieWords } = await getWords(URL);

      expect(trieWords.includes(word)).toBe(true);
    });
  });
});

describe('Delete words', () => {
  words.forEach(word => {
    test(`Deleting the word ${word}`, async () => {
      await deleteWord(URL, word);

      const { words: trieWords } = await getWords(URL);

      expect(trieWords.includes(word)).toBe(false);
    });
  });
});

describe('Search words', () => {
  poppedWords = getAndPopWord(words, Math.round(words.length / 2) - 1);

  test('Seeding the Trie', async () => {
    for (let word of words) {
      await addWord(URL, word);
    }
  });

  poppedWords.forEach(word => {
    test(`Searching deleted word: ${word}`, async () => {
      const { result } = await searchWord(URL, word);

      expect(result).toBe(false);
    });
  });

  words.forEach(word => {
    test(`Searching existing word: ${word}`, async () => {
      const { result } = await searchWord(URL, word);

      expect(result).toBe(true);
    });
  });
});

describe('Autocomplete', () => {
  words.forEach(word => {
    test(`Autocompleting word: ${word}`, async () => {
      let prefix = getRandomPrefix(word);

      const { result } = await autocompleteWord(URL, prefix);

      const autocp = words.filter(w => w.startsWith(prefix));

      expect(result).toStrictEqual(autocp);
    });
  });
});

describe('Reset to default again', () => {
  test('Resetting', async () => {
    await resetTrie(URL);

    const { words: trieWords } = await getWords(URL);
    expect(trieWords).toEqual([]);
  });
});
