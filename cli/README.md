# Trie CLI

This project consists of 2 components.

- Server: For manipulating the global trie through it's API.
- CLI: For Client usage. This allows users to Add, Remove, Search, Autocomplete and Print the trie.

## CLI

### Installation
The CLI is created using Node.js. In order to install this, you need `npm`. Download Node.js with npm
[here](https://nodejs.org/en/download/).

Here are the steps to install the CLI,

- Switch to the CLI folder: `cd cli`
- Install all the dependencies: `npm install`
- Install the CLI globally: `npm install -g .`

Once done, You can use it globally everywhere! Here is the help command, `trie-cli --help`.

### Usage
The command syntax for the CLI is `trie-cli <command> [arguments]`

Here are the commands that are available.
```
add <word>          Add a word to the Global trie
delete <word>       Delete a word from the Global trie
search <word>       Search if a word exists in the Trie and Returns True/False
autocomplete <word> Returns all the words present in the Trie starting with the specified word prefix
display             Display the trie in a prettified visual form
reset               Reset the Trie to initial state
```

There are automated tests for the CLI using Jest package. You can run the tests using the following
commands,
- Switch to the CLI folder: `cd cli`
- Ensure you have all dependencies installed using `npm install`
- Run the tests: `npm test`

### Tech stack

For the CLI, I am using `Node.js`.

Here are the libraries, that I used to make this CLI happen.

- `axios`: A powerful library for fetching APIs and web-based things.
- `chalk`: An amazing library for colored console output.
- `yargs`: A really handy library to easily build CLIs using Node.
