#!/usr/bin/env node

// Imports
const chalk = require('chalk');
const yargs = require('yargs');

const { URL } = require('../config');
const {
  addWord,
  deleteWord,
  searchWord,
  autocompleteWord,
  getWords,
  resetTrie
} = require('./utils');
const { prettyPrintTrie } = require('./trie');


// Usage
const usage = '\nUsage: trie-cli <command> [args]';
const options = yargs
  .usage(usage)
  .command(
    'add <word>',
    'Add a word to the trie',
    yargs => {
      yargs.positional('word', {
        description: 'The word to add to the trie',
        type: 'string'
      });
    },
    async argv => {
      await addWord(URL, argv.word);
      console.log(chalk.green(`Successfully inserted ${argv.word} into the trie.`));
    }
  )
  .command(
    'delete <word>',
    'Delete a word from the trie',
    yargs => {
      yargs.positional('word', {
        description: 'The word to delete from the trie',
        type: 'string'
      });
    },
    async argv => {
      await deleteWord(URL, argv.word);
      console.log(chalk.green(`Successfully deleted ${argv.word} from the trie.`));
    }
  )
  .command(
    'search <word>',
    'Search if a word exists in the trie',
    yargs => {
      yargs.positional('word', {
        description: 'The word to search in the trie',
        type: 'string'
      });
    },
    async argv => {
      const { result } = await searchWord(URL, argv.word);

      if (result) {
        console.log(chalk.green(`The word ${argv.word} exists in the trie.`));
      } else {
        console.log(chalk.red(`The word ${argv.word} does not in the trie.`));
      }
    }
  )
  .command(
    'autocomplete <word>',
    'Get autocomplete for words based on the word-prefix given.',
    yargs => {
      yargs.positional('word', {
        description: 'The word-prefix to get autocomplete for words in trie.',
        type: 'string'
      });
    },
    async argv => {
      const { result } = await autocompleteWord(URL, argv.word);

      if (result.length !== 0) {
        console.log(
          chalk.green(`Autocomplete suggestions for ${argv.word}: ${result}`)
        );
      } else {
        console.log(
          chalk.red(
            `No autocomplete suggestions found for the word ${argv.word}`
          )
        );
      }
    }
  )
  .command('display', 'Display the trie in a pretty manner', async argv => {
    const { words } = await getWords(URL);

    prettyPrintTrie(words);
  })
  .command('reset', 'Reset the whole trie to the initial state', async argv => {
    await resetTrie(URL);
    console.log(chalk.green('Successfully reset the trie!'));
  })
  .help(true)
  .parse();
