const axios = require('axios');
const chalk = require('chalk');

const addWord = async (url, word) => {
  try {
    const response = await axios.post(url + '/insert', {
      word: word
    });
    return response.data;
  } catch (error) {
    console.log(chalk.red('Failed to insert the word into the trie.'));
  }
};

const deleteWord = async (url, word) => {
  try {
    const response = await axios.post(url + '/delete', {
      word: word
    });
    return response.data;
  } catch (error) {
    console.log(chalk.red('Failed to delete the word from the trie.'));
  }
};

const searchWord = async (url, word) => {
  try {
    const response = await axios.post(url + '/search', {
      word: word
    });
    return response.data;
  } catch (error) {
    console.log(chalk.red('Error occured while searching!'));
  }
};

const autocompleteWord = async (url, word) => {
  try {
    const response = await axios.post(url + '/autocomplete', {
      word: word
    });
    return response.data;
  } catch (error) {
    console.log(chalk.red('Error occured while searching!'));
  }
};

const getWords = async url => {
  try {
    const response = await axios.get(url + '/words');
    return response.data;
  } catch (error) {
    console.log(
      chalk.red('Error occurred while fetching the words from trie!')
    );
  }
};

const resetTrie = async url => {
  try {
    const response = await axios.get(url + '/reset');
    return response.data;
  } catch (error) {
    console.log(chalk.red('Error occured while resetting!'));
  }
};

module.exports = {
  addWord,
  deleteWord,
  searchWord,
  autocompleteWord,
  getWords,
  resetTrie
};
