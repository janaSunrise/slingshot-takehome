const chalk = require('chalk');

const createTrieObject = words => {
  const trie = { children: {}, endOfWord: false };

  for (let i = 0; i < words.length; i++) {
    insertWordIntoTrie(words[i], trie);
  }

  return trie;
};

const insertWordIntoTrie = (word, trie) => {
  if (word.length === 0) {
    trie.endOfWord = true;
    return;
  }

  if (!trie.children[word[0]]) {
    trie.children[word[0]] = { children: {}, endOfWord: false };
  }

  // Recursion
  insertWordIntoTrie(word.slice(1), trie.children[word[0]]);
};

function prettyPrintTrie(words, prefix = '') {
  const trie = createTrieObject(words);
  printPrettyTrieHelper(trie, prefix);
}

const printPrettyTrieHelper = (trie, prefix) => {
  const words = Object.keys(trie.children).sort();
  const nodeName = 'Root Node';

  // Spacing
  let spacing = '';
  for (let i = 0; i <= prefix.length; i++) {
    spacing += ' ';
  }

  // Display word in green, else iteration in yellow
  if (trie.endOfWord) {
    console.log(chalk.green(`${spacing}•${prefix || nodeName}`));
  } else {
    console.log(chalk.yellow(`${spacing}${prefix || nodeName}`));
  }

  for (let i = 0; i < words.length; i++) {
    printPrettyTrieHelper(trie.children[words[i]], prefix + words[i]);
  }
};

module.exports = { prettyPrintTrie };
