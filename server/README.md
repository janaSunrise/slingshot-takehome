# Trie Server

This server hosts the global trie, that is accessed by the Node.js CLI created by me.

## Installation

This server makes use of Python along with a lightweight web framework called flask.
I have used `pipenv` for the virtual environment here.

If you don't have `pipenv` installed, you can do so using `pip install pipenv`.

Here are the steps to install and get the server up and running.

- Ensure you are in the `server` folder.
- Install all the dependencies using `pipenv sync -d`
- Run the server using `pipenv run start`

The server should start in a second or two on [http://localhost:5000](http://localhost:5000).

## Features and info

The trie has been written in a way to support all the amazing features together. Here is what's inside!

- Automatic removal of anything other than Alphabets
- Easy insertion
- Deletion of words
- Searching through for words
- Quick and efficient autocompletion
- Elegant export feature of the Trie to JSON format
- Easily serialize and load the Trie from database (`sqlite` used).

The server is currently hosted on heroku, [here](https://global-trie-sunrit.herokuapp.com/).

## Tech stack

This project is really lightweight and simple. This uses the flask web framework for building the API 
endpoints, and It does the job really well.

This has 2 components,

- Rest gateway to interact with the trie through API
- Modular trie to easily combine with API to provide a robust experience

## Interaction of Client and Server

The trie has been designed in a way such that the API endpoints when called (`GET`) or info is sent (`POST`)
based on endpoint, It's handled efficiently to provide a smooth experience.

The endpoints for JSON structure and words in the trie enables GET method, and Rest uses POST in order to
get the word so that the trie can process it, and send the response.

The error handling has been done for both Trie structure and the API, such that if the JSON response
passed during the POST request has the `word` variable mandatory to process, All the such edge cases
has been handled properly.

The Data structure of the Trie has been made in such a way to use more of things from scratch to
show my algorithmical thinking knowledge. I have made heavy use of recursion to simplify my work along
with doing the work in a clean approach.

## JSON Conversion and Database serialization

These are two handy features in order to ensure that it works fine when passing from one API to another
using JSON, and Database to save it time-to-time, and load it later, if the server is restarted with the
existing state.

I have designed the JSON export in a clean and understandable way for parsing.
Let's say, We have 3 words, Hi, He, Hello. Here is how the JSON would be like,

```json
{
   "root":{
      "h":{
         "e":{
            "end_of_word":true,
            "l":{
               "l":{
                  "o":{
                     "end_of_word":true
                  }
               }
            }
         },
         "i":{
            "end_of_word":true
         }
      }
   }
}
```

`end_of_word` shows that the word has ended.

Now, the database. It has a table `words` with a single column `word` of type TEXT. We export all the words
from the Trie, and Start saving them one by one. While loading, all the words are loaded, and iterated, while
inserting into the global trie object one by one. Really simple but handy!

## REST Endpoints present

Here are the list of the REST endpoints. All the endpoints are located under the namespace of `/api`, That 
is if the endpoint is `/test`, That expands to `<host>/api/test`.

**Insert: `/insert`**

This is a POST endpoint, and Takes the JSON body of the following,
```json
{
    "word": "{my-word}"
}
```

It inserts the word into the trie and returns a Status code of 200, if successful and a 
JSON of `{"status": "success"}`.

**Delete: `/delete`**

This POST endpoint takes the JSON body of same as Above, and deletes the specific word if it exists
in the trie.

**Search: `/search`**

This POST endpoint takes the JSON body with the word, and Searches through trie to check if the
specific word exists.

It returns the following JSON
```json
{"result": true}
```
If exists, else result is false.

**Autocomplete: `/autocomplete`**

This POST endpoint takes the JSON body with word, and Gets the words in the trie that starts with
the specified word as prefix to find.

It return the following JSON
```json
{"result": ["a", "aa", "aaa"]}
```
If found, else empty list if none is found.

**JSON export: `/trie`**

This GET endpoint gives the JSON format of the Trie with the current state. Here is how it can look like,
```json
{
   "root":{
      "h":{
         "e":{
            "end_of_word":true,
            "l":{
               "l":{
                  "o":{
                     "end_of_word":true
                  }
               }
            }
         },
         "i":{
            "end_of_word":true
         }
      }
   }
}
```

**Words in the trie: `/words`**

This GET endpoint returns the list of words present in the trie. Assuming we have Hi, Hey, and Hello in the
Trie, It would return this following JSON,

```json
{"words": ["hi", "hey", "hello"]}
```

**Reset: `/reset`**

This GET endpoints resets the Trie to it's initial state.

## Future improvements

If I had the chance to work more on this project and improve it more, I would do these changes,

- Add better JSON responses
- Make better tests for testing
- Create and Use a custom Queue service to serve the needs of a single client at once, with others in the
  pending state

