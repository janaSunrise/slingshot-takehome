# Import the flask app from __init__
from . import app

# Run the app
if __name__ == "__main__":
    app.run(debug=True)
