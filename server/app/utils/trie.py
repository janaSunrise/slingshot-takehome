import re
import sqlite3


class TrieNode:
    def __init__(self) -> None:
        # Children nodes
        self.children = {}

        # End of word?
        self.end_of_word = False


class Trie:
    def __init__(self) -> None:
        # Root node
        self.root = TrieNode()

    # Utility functions
    def get_alphabets(self, word: str):
        return "".join(re.findall(r"[a-zA-Z]+", word)).lower()

    def is_node_empty(self, node: TrieNode) -> bool:
        return len(node.children) == 0

    # Data structure operations
    def insert(self, word: str) -> None:
        node = self.root

        # Clean the word
        word = self.get_alphabets(word)

        # Traverse through all characters of given word
        for char in word:
            # Create new node if not present
            if char not in node.children:
                node.children[char] = TrieNode()

            # Move to next node
            node = node.children[char]

        # Mark current node as leaf
        node.end_of_word = True

    def search(self, word: str) -> bool:
        node = self.root

        # Clean the word
        word = self.get_alphabets(word)

        # Iterate over the word's characters
        for char in word:
            if char not in node.children:
                return False

            # Move to next node
            node = node.children[char]

        # If current node is leaf node
        if node.end_of_word:
            return True

        return False

    def autocomplete(self, prefix: str) -> list:
        node = self.root

        # Clean the word
        prefix = self.get_alphabets(prefix)

        output = []

        # Iterate over the word's characters
        for char in prefix:
            if char not in node.children:
                return output

            # Move to next node
            node = node.children[char]

        self._autocomplete(node, prefix, output)
        return output

    # Recursively get all the words with the prefix
    def _autocomplete(self, node: TrieNode, prefix: str, output: list) -> None:
        # If current node is leaf node
        if node.end_of_word:
            output.append(prefix)

        # If current node has children
        if not self.is_node_empty(node):
            for char in node.children:
                self._autocomplete(node.children[char], prefix + char, output)

    def delete(self, word: str) -> None:
        node = self.root

        # Clean the word
        word = self.get_alphabets(word)

        # Iterate over the word's characters
        for char in word:
            if char not in node.children:
                return

            # Move to next node
            node = node.children[char]

        # If current node is leaf node
        if node.end_of_word:
            node.end_of_word = False

            # If current node has children
            if not self.is_node_empty(node):
                return

            # Delete current node
            del node
        return

    # Convert the trie into JSON recursively for all words
    def export_trie_to_json(self) -> dict:
        return {"root": self.to_json(self.root)}

    def to_json(self, node: TrieNode) -> dict:
        json = {}

        if node.end_of_word:
            json["end_of_word"] = True

        # Get children
        if not self.is_node_empty(node):
            for char in node.children:
                json[char] = self.to_json(node.children[char])

        return json

    # Database functionalities
    def save_to_db(self, db_name: str) -> None:
        conn = sqlite3.connect(db_name)
        c = conn.cursor()

        c.execute("DROP TABLE IF EXISTS words")
        c.execute("CREATE TABLE words (word TEXT)")

        self._save_to_db(self.root, "", c)
        conn.commit()
        conn.close()

    def _save_to_db(self, node: TrieNode, prefix: str, c: sqlite3.Cursor) -> None:
        # If current node is leaf node
        if node.end_of_word:
            c.execute("INSERT INTO words VALUES (?)", (prefix,))

        # If current node has children
        if not self.is_node_empty(node):
            for char in node.children:
                self._save_to_db(node.children[char], prefix + char, c)

    def load_from_db(self, db_name: str) -> None:
        conn = sqlite3.connect(db_name)
        c = conn.cursor()

        c.execute("SELECT * FROM words")
        for row in c:
            self.insert(row[0])

        conn.close()

    def get_words(self) -> list:
        # Define word list
        words = []

        # Recursively gather all words
        self._get_words(self.root, "", words)

        # Return word list
        return words

    def _get_words(self, node: TrieNode, prefix: str, words: list) -> None:
        # If current node is leaf node
        if node.end_of_word:
            words.append(prefix)

        # If current node has children
        if not self.is_node_empty(node):
            for char in node.children:
                self._get_words(node.children[char], prefix + char, words)

    def reset(self) -> None:
        self.root = TrieNode()
