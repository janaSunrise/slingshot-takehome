from flask import Flask, jsonify, request

from .utils.trie import Trie

# Initialize the app
app = Flask(__name__)

trie = Trie()


# Decorator to ensure "word" exists in JSON post request
def word_exists(func):
    def wrapper(*args, **kwargs):
        word = request.json.get("word")
        if word is None:
            return jsonify({"error": "Missing word"}), 400

        return func(*args, **kwargs)

    return wrapper


# The views
@word_exists
@app.route("/api/insert", methods=["POST"])
def insert():
    word = request.json.get("word")
    trie.insert(word)

    return jsonify({"status": "success"})


@word_exists
@app.route("/api/delete", methods=["POST"])
def delete():
    word = request.json.get("word")
    trie.delete(word)

    return jsonify({"status": "success"})


@word_exists
@app.route("/api/search", methods=["POST"])
def search():
    word = request.json.get("word")
    result = trie.search(word)

    return jsonify({"result": result})


@word_exists
@app.route("/api/autocomplete", methods=["POST"])
def autocomplete():
    word = request.json.get("word")
    result = trie.autocomplete(word)

    return jsonify({"result": result})


@app.route("/api/reset")
def reset_reset():
    trie.reset()

    return jsonify({"status": "success"})


@app.route("/api/trie")
def trie_view():
    return jsonify(trie.export_trie_to_json())


@app.route("/api/words")
def words_view():
    return jsonify({"words": trie.get_words()})
