# Trie Server and CLI app

This project consists of 2 components.

- Server: For manipulating the global trie through it's API.
- CLI: For Client usage. This allows users to Add, Remove, Search, Autocomplete and Print the trie.

For separate documentation about CLI or Server, Check the respective READMEs in their folder.

**Quick note**: I have tested this on my machine and VPS, and It works perfectly. Due to heroku's memory
limits, It does not always work as intended. I encourage you to change `const DEV_MODE = true;` in
`config.js` to true, Spin up the server, and Test it using local instances.

## CLI

### Installation
The CLI is created using Node.js. In order to install this, you need `npm`. Download Node.js with npm
[here](https://nodejs.org/en/download/).

Here are the steps to install the CLI,

- Switch to the CLI folder: `cd cli`
- Install all the dependencies: `npm install`
- Install the CLI globally: `npm install -g .`

Once done, You can use it globally everywhere! Here is the help command, `trie-cli --help`.

### Usage
The command syntax for the CLI is `trie-cli <command> [arguments]`

Here are the commands that are available.
```
add <word>          Add a word to the Global trie
delete <word>       Delete a word from the Global trie
search <word>       Search if a word exists in the Trie and Returns True/False
autocomplete <word> Returns all the words present in the Trie starting with the specified word prefix
display             Display the trie in a prettified visual form
reset               Reset the Trie to initial state
```

There are automated tests for the CLI using Jest package. You can run the tests using the following
commands,
- Switch to the CLI folder: `cd cli`
- Ensure you have all dependencies installed using `npm install`
- Run the tests: `npm test`

### Tech stack

For the CLI, I am using `Node.js`.

Here are the libraries, that I used to make this CLI happen.

- `axios`: A powerful library for fetching APIs and web-based things.
- `chalk`: An amazing library for colored console output.
- `yargs`: A really handy library to easily build CLIs using Node.

## Server

## Features and info

The trie has been written in a way to support all the amazing features together. Here is what's inside!

- Automatic removal of anything other than Alphabets
- Easy insertion
- Deletion of words
- Searching through for words
- Quick and efficient autocompletion
- Elegant export feature of the Trie to JSON format
- Easily serialize and load the Trie from database (`sqlite` used).

The server is currently hosted on heroku, [here](https://global-trie-sunrit.herokuapp.com/).

## Tech stack

This project is really lightweight and simple. This uses the flask web framework for building the API 
endpoints, and It does the job really well.

This has 2 components,

- Rest gateway to interact with the trie through API
- Modular trie to easily combine with API to provide a robust experience
